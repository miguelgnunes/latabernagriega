<?php /* Template Name: No slider */

get_header("noslider"); ?>

	<div class="full-main-section my-grey" style="position: relative">
		<div style="position: absolute; height: 46px; width: 100%; background-color: white; top: 0"></div>
		<div style="position: absolute; height: 26px; width: 100%; background-color: white; bottom: 0"></div>
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );


			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->


	</div><!-- full-main-section -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
