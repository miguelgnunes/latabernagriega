<?php /* Template Name: No slider no title */

get_header("noslider"); ?>
	<div class="main-section" style="position: relative">
		<div style="position: absolute; height: 100%; top: 0; width: 16.66666667%; left: -9px" class="right-standard-border"></div>
		<div style="position: absolute; height: 100%; top: 0; width: 16.66666667%; right: -9px" class="left-standard-border"></div>
		<div class="fluid-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2" style="position: relative">
					<div class="content-piece content-piece-padding">
						<div style="position: absolute; height: 100%; top: 0; left: 50%" class="right-standard-border"></div>
						<div class="reservation-container" style="position: relative">
							<center>
								<header>
									<?php the_title( '<p class="central-header">', '</p>' ); ?>
									<p class="central-subheader"><?php the_subtitle(); ?></p>
								</header><!-- .entry-header -->
							</center>
							<div class="row">
								<div class="col-md-6 hidden-xs" style="padding: 29px 96px">
									<img style="width: 100%" src="<?php echo get_stylesheet_directory_uri();?>/images/chair.png">
								</div>
								<div class="col-md-6" style="padding: 10px 50px">
									<?php echo do_shortcode("[huge_it_forms id='4']"); ?>
								</div>

							</div>
							<center><button type="submit" form="huge_it_contact_form_4" class="taberna-button"><div><?php _e("<!--:es-->RESERVAR<!--:en-->BOOK"); ?></div></button></center>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="full-main-section" style="position: relative">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();


			// Include the page content template.



			get_template_part( 'template-parts/content_notitle', 'page' );


			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->


	</div><!-- full-main-section -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
