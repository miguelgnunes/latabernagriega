<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>

	<link href='https://fonts.googleapis.com/css?family=Marcellus' rel='stylesheet' type='text/css'>	
</head>

<body <?php body_class(); ?>>
	<div id="main" style="display: none">
		<div class="full-main-section">
			<div id="responsive-fill" class="my-blue"></div>
			<div class="main-section">
				<div class="row">
					<!-- <div align="right" class="col-xs-2 right-standard-border" style="padding-top: 10px"> -->
					<div align="right" class="col-xs-2 menu-col header-menu" style="padding-top: 10px">
						<div class="menu-item menu-item-text-top">
							<?php dynamic_sidebar( 'language-sidebar' ); ?>
						</div>
						<div class="menu-item"><a href="https://www.facebook.com/La-Taberna-Griega-Bcn-356121964459230/?fref=ts" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-fb.png" style="height: 15px; float: left">FACEBOOK</a></div>
						<div class="menu-item menu-item-text-top top-standard-border"><a href="https://www.facebook.com/La-Taberna-Griega-Bcn-356121964459230/?fref=ts" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-insta.png" style="height: 15px; float: left">INSTAGRAM</a></div>
					</div>
					<div class="col-xs-8 right-standard-border left-standard-border">
						<center>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<img id="logo" src="<?php header_image(); ?>">
							</a>
						</center>
					</div>
					<div class="col-xs-2 menu-col" style="padding-top: 10px">
						<?php if ( has_nav_menu( 'primary' ) ) : ?>
							<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
								<?php
									wp_nav_menu( array(
										'theme_location' => 'primary',
										'menu_class'     => 'header-menu',
									 ) );
								?>
							</nav><!-- .main-navigation -->
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div id="content">