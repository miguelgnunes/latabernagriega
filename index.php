<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<?php 
	$weekday_es = array('DOMINGO', 'LUNES', 'MARTES', 'MIERCOLES', 'JUEVES', 'VIERNES', 'SABADO');
	$weekday_en = array('SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY','THURSDAY','FRIDAY', 'SATURDAY');
	$months_es = array('ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE');
	$months_en = array('JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER');
	$weekindex =  date("w");
	$day =  date("d");
	$monthindex =  intval(date("m")) - 1;
?>
<div class="full-main-section" style="position: relative">
	<div class="main-section" style="position: relative">
		<div style="position: absolute; height: 100%; top: 0; width: 16.66666667%; left: -9px" class="right-standard-border"></div>
		<div style="position: absolute; height: 100%; top: 0; width: 16.66666667%; right: -9px" class="left-standard-border"></div>
		<div class="content-piece" style="padding-bottom: 50px; margin-top: -15px">
			<div style="position: absolute; height: 100%; top: 0; left: 50%" class="right-standard-border"></div>
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1">
					<div id="menudeldia-container">
						<center>
							<header>
								<p class="central-header"><?php _e("<!--:es-->MENÚ DEL DIA<!--:en-->MENU OF THE DAY"); ?></p>
								<p class="central-subheader">ΚΑΤΆΛΟΓΟΣ ΤΗΣ ΗΜΈΡΑΣ</p>
							</header><!-- .entry-header -->
						</center>
						<div class="row">
							<div class="col-xs-4 col-xs-offset-2">
								<div style="padding: 29px 96px">
									<img style="width: 100%;" src="<?php echo get_stylesheet_directory_uri();?>/images/chair.png">
								</div>
								<center>
									<p style="font-size: 20px"><?php _e("<!--:es-->" . $weekday_es[$weekindex] . "<!--:en-->" . $weekday_en[$weekindex]);
									echo " " . $day . " "; 
									_e("<!--:es-->DE<!--:en--> "); 
									echo " "; 
									_e("<!--:es-->" . $months_es[$monthindex] . "<!--:en-->" . $months_en[$monthindex]); ?> </p>
									<P style="font-size: 20px"><?php _e("<!--:es-->DE 13 A 16:30H<!--:en-->FROM 13 TO 16:30H"); ?></P>
								</center>
							</div>
							<div class="col-xs-4" style="padding: 10px 50px; margin-top: 19px">
								<center>
									<?php dynamic_sidebar( 'menudeldia-sidebar' ); ?>
								</center>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="full-main-section my-grey" style="position: relative">
	<div style="position: absolute; height: 46px; width: 100%; background-color: white; top: 0"></div>
	<div style="position: absolute; height: 26px; width: 100%; background-color: white; bottom: 0"></div>
	<div class="main-section" style="position: relative">
		<div style="position: absolute; height: 100%; top: 0; width: 16.66666667%; left: -9px" class="right-standard-border"></div>
		<div style="position: absolute; height: 100%; top: 0; width: 16.66666667%; right: -9px" class="left-standard-border"></div>
		<center>
			<header>
				<p class="central-header"><?php _e("<!--:es-->LA CARTA<!--:en-->THE MENU"); ?></p>
				<p class="central-subheader">ΚΑΤΆΛΟΓΟΣ ΤΗΣ ΗΜΈΡΑΣ</p>
			</header><!-- .entry-header -->
		</center>
		<div class="content-piece content-piece-padding">
			<div style="position: absolute; height: 100%; top: 0; left: 50%" class="right-standard-border"></div>
			<div class="row">
				<div class="col-xs-2">
					<p class="index-lacarta-header"><?php _e("<!--:es-->Especialidades de Grecia<!--:en-->Specialties from Greece"); ?></p>
					<p class="content-text"><?php _e("<!--:es-->Especialidades como la pikilla; ensaladas: horatiki salata, saganaki me salata; y platos como: moussaka, kokkinisto, kolokithakia y kalamaria entre otros,
sin olvidar sus vinos griegos y postres caseros.<!--:en-->Especialidades como la pikilla; ensaladas: horatiki salata, saganaki me salata; y platos como: moussaka, kokkinisto, kolokithakia y kalamaria entre otros,
sin olvidar sus vinos griegos y postres caseros."); ?></p>
					<p class="index-lacarta-header gold-text" style="margin-top: 80px"><?php _e("<!--:es-->Menú para grupos<!--:en-->Groups' menu"); ?></p>
					<p class="content-text gold-text"><?php _e("<!--:es-->Con un mínimo de dos comensales, es ideal para cenas de empresa, grandes familias o cenas en pareja,<!--:en-->Con un mínimo de dos comensales, es ideal para cenas de empresa, grandes familias o cenas en pareja,"); ?></p>
				</div>
				<div class="col-xs-8">
					<img style="width: 100%; margin-bottom: 40px" src="<?php echo get_stylesheet_directory_uri();?>/images/lacarta-main.jpg">
					<center style="position: absolute;left: 0;right: 0;margin-left: auto;margin-right: auto;"><a href="<?php _e("<!--:es-->la-carta<!--:en-->en/la-carta"); ?>"><button class="taberna-button" style="background-color: white"><div><?php _e("<!--:es-->VER LA CARTA<!--:en-->SEE MENU"); ?></div></button></a></center>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$page_id = get_page_by_path("rotura-platos");
if(get_post_status($page_id) == "publish" ) {

$post = get_post($page_id);
$title = get_the_title($post);
$content = $post->post_content;
// $content = apply_filters('the_content', $content);
// $content = str_replace(']]>', ']]>', $content);
// echo $content;
	?>
<div class="full-main-section" style="position: relative">
	<div class="main-section" style="position: relative">
		<div style="position: absolute; height: 100%; top: 0; width: 16.66666667%; left: -9px" class="right-standard-border"></div>
		<div style="position: absolute; height: 100%; top: 0; width: 16.66666667%; right: -9px" class="left-standard-border"></div>
		<div class="content-piece" style="padding-bottom: 50px; margin-top: -15px">
			<div style="position: absolute; height: 100%; top: 0; left: 50%" class="right-standard-border"></div>
			<div style="height: 50px;"></div>
		</div>
		<center>
			<header>
				<p class="central-header"><?php _e("<!--:es-->ROTURA DE PLATOS<!--:en-->ROTURA DE PLATOS"); ?></p>
				<p class="central-subheader">ΘΡΑΎΣΜΑΤΑ ΠΙΝΑΚΊΔΩΝ</p>
			</header><!-- .entry-header -->
		</center>
		<div class="content-piece" style="padding-bottom: 50px">
			<div style="position: absolute; height: 100%; top: 0; left: 50%" class="right-standard-border"></div>
			<div class="row">
				<div class="col-xs-2">
					<p class="index-roturaplatos-header"><?php echo $title; ?></p>
					<p class="index-roturaplatos-text"><?php echo $content; ?></p>
				</div>
				<div class="col-xs-8" style="max-height: 300px; overflow-y: hidden">
					<img style="width: 50%; float: left" src="<?php echo get_stylesheet_directory_uri();?>/images/baile.png">
					<img style="width: 50%; float: left" src="<?php echo get_stylesheet_directory_uri();?>/images/bailando.jpg">
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
}
?>
<?php get_footer(); ?>
