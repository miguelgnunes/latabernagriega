<?php
function theme_enqueue_styles() {

    // wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css'
    );
    wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css' );
    // wp_enqueue_script( 'jquery', get_stylesheet_directory_uri() . '/js/jquery.min.js', array());
    wp_enqueue_script('smooth-state', get_stylesheet_directory_uri() . '/js/jquery.smoothState.min.js', array('jquery'));
    wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/myjs.js', array('jquery', 'smooth-state'));

}
// add_action( 'after_setup_theme', 'menus_setup');
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function remove_default_sidebars(){

    // Unregister twentysixteen sidebars
    unregister_sidebar( 'sidebar-1' );
    unregister_sidebar( 'sidebar-2' );
    unregister_sidebar( 'sidebar-3' );
}
add_action( 'widgets_init', 'remove_default_sidebars', 11);

function twentysixteen_child_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Language Sidebar', 'twentysixteen-child' ),
        'id'            => 'language-sidebar',
        'description'   => __( 'For the language widget', 'twentysixteen-child' ),
        'before_widget' => '<div id="ola">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => '',
    ) );
    register_sidebar( array(
        'name'          => __( 'Widget Menu del dia', 'twentysixteen-child' ),
        'id'            => 'menudeldia-sidebar',
        'description'   => __( 'Para presentar los platos del menu del dia', 'twentysixteen-child' ),
        'before_widget' => '<div id="menudeldia-widget-container">',
        'after_widget'  => '</div>',
        'before_title'  => '<p class="content-text">',
        'after_title'   => '</p>',
    ) );
}
add_action( 'widgets_init', 'twentysixteen_child_widgets_init' );

// function create_post_type() {
//     register_post_type( 'plato',
//         array(
//         'labels' => array(
//         'name' => __( 'Platos' ),
//         'singular_name' => __( 'Plato' ),
//         'menu_name' => __( 'Menu del dia' ),
//         ),
//         'public' => true,
//         'has_archive' => true,
//         'menu_position' => 20,
//         'menu_icon' => 'dashicons-carrot'
//         )
//         );
// }
// add_action( 'init', 'create_post_type' );
// function change_platos_support() {
//     remove_post_type_support( 'plato', 'post-formats' );
//     remove_post_type_support( 'plato', 'page-attributes' );
//     remove_post_type_support( 'plato', 'comments' );
//     remove_post_type_support( 'plato', 'editor' );
//     remove_post_type_support( 'plato', 'thumbnail' );
//     add_post_type_support( 'plato', 'excerpt' );
// }
// add_action( 'init', 'change_platos_support', 10 );
?>