<?php /* Template Name: No slider no title */

get_header("noslider"); ?>
	<div class="main-section" style="position: relative">
		<div style="position: absolute; height: 100%; top: 0; width: 16.66666667%; left: -9px" class="right-standard-border"></div>
		<div style="position: absolute; height: 100%; top: 0; width: 16.66666667%; right: -9px" class="left-standard-border"></div>
		<div class="fluid-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2" style="position: relative">
					<div class="content-piece content-piece-padding" style="">
						<div style="position: absolute; height: 100%; top: 0; left: 50%" class="right-standard-border"></div>
						<div style="width: 100%; height: 400px; overflow-y: hidden; position: relative; -webkit-filter: grayscale(1); filter: grayscale(1);">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5985.35238435078!2d2.157382!3d41.402838!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a2bd16d34799%3A0x5ff6ff2e166baa29!2sCarrer+del+Torrent+de+l&#39;Olla%2C+123%2C+08012+Barcelona%2C+Espanha!5e0!3m2!1spt-PT!2spt!4v1453855911198" width="100%" height="550" frameborder="0" style="border:0; margin-top: -150px" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="full-main-section" style="position: relative">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();


			// Include the page content template.



			get_template_part( 'template-parts/content', 'page' );


			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->


	</div><!-- full-main-section -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
