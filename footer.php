	
		</div><!-- end content -->
		<div class="full-main-section">
			<div class="main-section" style="margin-top: 10px">
				<div class="row">
					<div class="col-xs-2 col-xs-offset-1">
						<center>
							<b style="font-family: Georgia; color: #001588; font-size: 14px">Contacto</b>
							<p class="footer-contact-item">TORRENT DE L’OLLA 123,</p>
							<p class="footer-contact-item">08012 BARCELONA</p>
							<p class="footer-contact-item">93 218 72 26</p>
							<div style="margin-top: 10px">
								<img style="height: 15px;" src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-fb.png">
								<a href="https://www.facebook.com/La-Taberna-Griega-Bcn-356121964459230/?fref=ts" target="_blank"><p class="footer-contact-item" style="display: inline">FACEBOOK</p></a>
							</div>
							<div>
								<img style="height: 15px;" src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-insta.png">
								<a href="https://www.facebook.com/La-Taberna-Griega-Bcn-356121964459230/?fref=ts" target="_blank"><p class="footer-contact-item" style="display: inline">INSTAGRAM</p></a>
							</div>
						</center>
					</div>
					<div class="col-xs-6">
						<center>
							<img id="logo-footer" src="<?php echo get_stylesheet_directory_uri() ?>/images/tabernagriega_logo_footer.png">
							<div style="position: relative; height: 50px; margin-top: 20px">
								<div style="position: absolute; height: 100%; top: 0; left: 50%" class="right-standard-border"></div>
							</div>
						</center>
					</div>
					<div class="col-xs-2">
						<center>
						<?php if ( has_nav_menu( 'primary' ) ) : ?>
							<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
								<?php
									wp_nav_menu( array(
										'theme_location' => 'primary',
										'menu_class'     => 'footer-menu',
									 ) );
								?>
							</nav><!-- .main-navigation -->
						<?php endif; ?>	
						</center>
					</div>
				</div>
			</div>
		</div>	
	</div> <!-- id=main -->
	<?php wp_footer(); ?>
</body>