<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<div id="post-<?php the_ID(); ?>" class="main-section" style="position: relative">
	<div style="position: absolute; height: 100%; top: 0; width: 16.66666667%; left: -9px" class="right-standard-border"></div>
	<div style="position: absolute; height: 100%; top: 0; width: 16.66666667%; right: -9px" class="left-standard-border"></div>

	<!-- <?php twentysixteen_post_thumbnail(); ?> -->

	<div class="content-piece content-piece-padding" style="margin-top: -15px; padding-top: 0">
		<div style="position: absolute; height: 100%; top: 0; left: 50%" class="right-standard-border"></div>
		<?php
		the_content();

		// wp_link_pages( array(
		// 	'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
		// 	'after'       => '</div>',
		// 	'link_before' => '<span>',
		// 	'link_after'  => '</span>',
		// 	'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
		// 	'separator'   => '<span class="screen-reader-text">, </span>',
		// ) );
		?>
	</div><!-- content-piece -->

	<?php
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
				get_the_title()
			),
			'<footer class="entry-footer"><span class="edit-link">',
			'</span></footer><!-- .entry-footer -->'
		);
	?>

</div><!-- #post-## -->
